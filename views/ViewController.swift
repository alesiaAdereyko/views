//
//  ViewController.swift
//  views
//
//  Created by Hackintosh on 1/3/19.
//  Copyright © 2019 Hackintosh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let v1 = UIView()
    
    var topConstraint: NSLayoutConstraint!
    var leadingConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .green
        button.setTitle("Press me", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        view.addSubview(button)
        
        button.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        button.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        button.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        v1.translatesAutoresizingMaskIntoConstraints = false
        v1.backgroundColor = .red
        view.addSubview(v1)
        
        v1.widthAnchor.constraint(equalToConstant: 100).isActive = true
        v1.heightAnchor.constraint(equalToConstant: 50).isActive = true
        v1.layoutIfNeeded()
        
        topConstraint = v1.topAnchor.constraint(equalTo: view.topAnchor, constant: (view.bounds.height - v1.bounds.height) / 2)
        topConstraint.isActive = true
        leadingConstraint = v1.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: (view.bounds.width - v1.bounds.width) / 2)
        leadingConstraint.isActive = true
        
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        v1.addGestureRecognizer(gestureRecognizer)
        
    }
    
    @objc func buttonAction(sender: UIButton!) {
        changeConstraints(movingView: v1, constantTop: (view.bounds.height - v1.bounds.height) / 2, constantLeading: (view.bounds.width - v1.bounds.width) / 2)
    }
    
    func changeConstraints(movingView: UIView, constantTop: CGFloat, constantLeading: CGFloat) {
        self.topConstraint.isActive = false
        self.leadingConstraint.isActive = false
        self.topConstraint = movingView.topAnchor.constraint(equalTo: view.topAnchor, constant: constantTop)
        self.topConstraint.isActive = true
        self.leadingConstraint = movingView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: constantLeading)
        self.leadingConstraint.isActive = true
    }
    
    var initialCenter = CGPoint()
    var newTranslation = CGPoint()
    @objc func handlePan(_ gestureRecognizer : UIPanGestureRecognizer) {
        guard let movingView = gestureRecognizer.view else {return}
        guard let width = movingView.superview?.bounds.width else {return}
        guard let height = movingView.superview?.bounds.height else {return}
        let translation = gestureRecognizer.translation(in: movingView.superview)
        if gestureRecognizer.state == .began {
            self.initialCenter = movingView.center
            self.newTranslation = initialCenter
        }
        
        if gestureRecognizer.state != .cancelled {
            let newCenter = CGPoint(x: initialCenter.x + translation.x, y: initialCenter.y + translation.y)
            if gestureRecognizer.state != .ended {
                newTranslation = CGPoint(x: newCenter.x - movingView.center.x, y: newCenter.y - movingView.center.y)
            }
            changeConstraints(movingView: movingView, constantTop: newCenter.y - movingView.bounds.height / 2, constantLeading: newCenter.x - movingView.bounds.width / 2)
        }

        if gestureRecognizer.state == .ended {
            UIView.animate(withDuration: 2.0, animations: {
                var newCenter: CGPoint?

                let k = self.newTranslation.y / self.newTranslation.x
                let b = movingView.center.y - (k * movingView.center.x)
                let vector = CGVector(dx: self.newTranslation.x, dy: self.newTranslation.y)
                
                let lineSegmentsData = [["begin": CGPoint(x: 0, y: 0), "end": CGPoint(x: width, y: 0), "interPoint": CGPoint(x: -b / k, y: 0)],
                                        ["begin": CGPoint(x: 0, y: 0), "end": CGPoint(x: 0, y: height), "interPoint": CGPoint(x: 0, y: b)],
                                        ["begin": CGPoint(x: width, y: 0), "end": CGPoint(x: width, y: height), "interPoint": CGPoint(x: width, y: k * width + b)],
                                        ["begin": CGPoint(x: 0, y: height), "end": CGPoint(x: width, y: height), "interPoint": CGPoint(x: (height - b) / k, y: height)]]
                
                for element in lineSegmentsData {
                    if self.isIntersect(k: k, b: b, interPoint: element["interPoint"]!) == true {
                        if self.isInLineSegment(interPoint: element["interPoint"]!, begin: element["begin"]!, end: element["end"]!) == true {
                            if self.isOneDirection(vector: vector, interPoint: element["interPoint"]!, origin: movingView.center) == true {
                                newCenter = element["interPoint"]
                                break
                            }
                        }
                    }
                    else {
                        continue
                    }
                }
                
                if self.newTranslation.x == 0 {
                    if self.newTranslation.y < 0 {
                        newCenter = CGPoint(x: movingView.center.x, y: 0)
                    }
                    else if self.newTranslation.y > 0 {
                        newCenter = CGPoint(x: movingView.center.x, y: height)
                    }
                    else {
                        newCenter = CGPoint(x:movingView.center.x, y: movingView.center.y)
                    }
                }
                
                if let newCenter = newCenter {
                    self.changeConstraints(movingView: movingView, constantTop: newCenter.y - movingView.bounds.height / 2, constantLeading: newCenter.x - movingView.bounds.width / 2)
                    movingView.superview?.layoutIfNeeded()
                }
            })
        }
    }
    
    func isIntersect (k: CGFloat, b: CGFloat, interPoint: CGPoint) -> Bool {
        if interPoint.y == k * interPoint.x + b {
            return true
        }
        else {
            return false
        }
    }
    
    func isInLineSegment (interPoint: CGPoint, begin: CGPoint, end: CGPoint) -> Bool {
        if interPoint.x == begin.x {
            if interPoint.y >= begin.y && interPoint.y <= end.y {
                return true
            }
            else {
                return false
            }
        }
        else {
            if interPoint.x >= begin.x && interPoint.x <= end.x {
                return true
            }
            else {
                return false
            }
        }
    }
    
    func isOneDirection (vector: CGVector, interPoint: CGPoint, origin: CGPoint) -> Bool {
        if (vector.dx < 0 && interPoint.x < origin.x) || (vector.dx > 0 && interPoint.x > origin.x) {
            return true
        }
        else if (vector.dx == 0) {
            if (vector.dy < 0 && interPoint.y < origin.y) || (vector.dy > 0 && interPoint.y > origin.y) {
                return true
            }
            else {
                return false
            }
        }
        else {
            return false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

